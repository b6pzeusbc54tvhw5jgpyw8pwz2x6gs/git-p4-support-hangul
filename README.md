# 1. clone 
```
./git_p4.py clone //SOME/YOUR/P4_PROJET/PATH@all gitProjectName
```


# 2. rebase
```
cd gitProjectName
../git_p4 rebase
```


## 3. 대부분의 커맨드는 p4 login을 필요로 한다
```
p4 login
```


## 4. P4 커밋템플릿
```
git config commit.template p4_commit_template
```


## 5. git 에서 한글파일을 핸들링하지 못하는 문제
```
git config --global core.quotepath false
```

## 6. git-p4로 import 한 git 프로젝트를 클론 후 다시 p4 와 연결시키기
```
## 마지막 p4 change list SHA 를 알아내서
$ git update-ref refs/remotes/p4/master 1a410efbd13591db07496601ebc7a059dd55cfe9
$ git symbolic-ref refs/remotes/p4/HEAD refs/remotes/p4/master

## check!
$ git branch --all
remotes/p4/HEAD -> p4/master
remotes/p4/master
```

## 7. 한글 파일이름 깨지는 문제. (Windows P4V, linux 에서 동시에 안깨지게 하기는 어려움)
```
git config --global git-p4.pathEncoding cp949
```
